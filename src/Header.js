import React from 'react';
import {BrowserRouter, Link} from "react-router-dom";
import Alert from 'react-bootstrap/Alert'
import styled from 'styled-components'

import 'bootstrap/dist/css/bootstrap.min.css';

export default function Header() {
    return (
        <BrowserRouter>
            <link
                rel="stylesheet"
                href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
                crossOrigin="anonymous"
            />
            {/*<Navbar bg="light" expand="lg">*/}
            {/*    <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>*/}
            {/*    <Navbar.Toggle aria-controls="basic-navbar-nav" />*/}
            {/*    <Navbar.Collapse id="basic-navbar-nav">*/}
            {/*        <Nav className="mr-auto">*/}
            {/*            <Nav.Link href="#home">Home</Nav.Link>*/}
            {/*            <Nav.Link href="#link">Link</Nav.Link>*/}
            {/*            <NavDropdown title="Dropdown" id="basic-nav-dropdown">*/}
            {/*                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>*/}
            {/*                <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>*/}
            {/*                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>*/}
            {/*                <NavDropdown.Divider />*/}
            {/*                <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>*/}
            {/*            </NavDropdown>*/}
            {/*        </Nav>*/}
            {/*        <Form inline>*/}
            {/*            <FormControl type="text" placeholder="Search" className="mr-sm-2" />*/}
            {/*            <Button variant="outline-success">Search</Button>*/}
            {/*        </Form>*/}
            {/*    </Navbar.Collapse>*/}
            {/*</Navbar>*/}
            <Link to="/Hello">Hello</Link>
            <Button >Click </Button>
            <Alert variant="success">
                <Alert.Heading>Hey, nice to see you</Alert.Heading>
                <p>
                    Aww yeah, you successfully read this important alert message. This example
                    text is going to run a bit longer so that you can see how spacing within an
                    alert works with this kind of content.
                </p>
                <hr/>
                <p className="mb-0">
                    Whenever you need to, be sure to use margin utilities to keep things nice
                    and tidy.
                </p>
            </Alert>
        </BrowserRouter>
    )
};
const Button = styled.button`
    width: 100px;
    height: 50px;
      font-size: 1.5em;
  background-color: black;
  color: white;
`;