import React, {useState} from 'react';

import Jumbotron from 'react-bootstrap/Jumbotron';
import Toast from 'react-bootstrap/Toast';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Form, FormControl} from "react-bootstrap";
// import '../App.css';
import {NewsPage} from "./NewsPage";

const NavMain = () => {
    return (
        <>

        </>
    );
};


export const News = () => (
    <>
        <Jumbotron>
            <h1 className="display-3">Breaking NEws~!</h1>
            <p className="lead">This is your News page where you can find interesting articles and more. Be Aware!</p>
            <hr className="my-2"/>
            <Button color="primary">Update</Button>
        </Jumbotron>

        <h1 className="header">Hello news</h1>
        <NewsPage/>
    </>
);