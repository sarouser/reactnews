// import React from 'react';
// import logo from './logo.svg';
// import './App.css';
//
// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }
//
// export default App;


import React, {useState} from 'react';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Jumbotron from 'react-bootstrap/Jumbotron';
import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Form, FormControl} from "react-bootstrap";
import '../App.css';
import {NavLink} from "react-router-dom";
import {Home} from "./Home";
import {News} from "./News";
import Navigation from "./Navigation";

//
// const NavMain = () => {
//     return (
//         {/*<Router>*/}
//         {/*    <Switch>*/}
//         {/*        <Route exact path = "/" component={}/>*/}
//         {/*        <Route path = "/News" component={}/>*/}
//         {/*    </Switch>*/}
//         {/*</Router>*/}
//
//         // <Navbar sticky="top" bg="light" expand="lg">
//         //     <Navbar.Brand href="">Home</Navbar.Brand>
//         //     <Navbar.Toggle aria-controls="basic-navbar-nav" />
//         //     <Navbar.Collapse id="basic-navbar-nav">
//         //         <Nav className="mr-auto">
//         //             <NavLink href="/News">News</NavLink>
//         //         </Nav>
//         //        {/* <Form inline>*/}
//         //     {/*        <FormControl type="text" placeholder="Search" className="mr-sm-2" />*/}
//         //     {/*        <Button variant="outline-success">Search</Button>*/}
//         //     {/*    </Form> */}
//         //     </Navbar.Collapse>
//         // </Navbar>
//         // </Router>
//     );
// };
//

const App = () => (
    <>
        <Navigation/>
        <Router>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route path="/News" component={News}/>
            </Switch>
        </Router>
    </>
);

export default App;