import React, {useState, useEffect} from 'react';
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {isStyledComponent} from "styled-components";
import styled from 'styled-components'
import {Button} from "react-bootstrap";
let sample;

const page = (props) => {
    return (
        <Container>
            <Button/>
            <Row>
                <Col><Container>{props}</Container></Col>
                <Col><Container>{props}</Container></Col>
                <Col>
                    <Container>{props}</Container>
                </Col>
                <Col><Container>{props}</Container></Col>
            </Row>
        </Container>
    )
};
let arr;
export const NewsPage = () => {

    const [newsList, setList] = useState([]);
    useEffect(() => {
        async function fetchData() {
            const res = await fetch("https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=502856e770a041b7a0c0c8de222b52ba");
            res
                .json()
                .then(res => {
                    setList(res.articles.map(x => x));
                    // console.log(res);
                })
                .catch(err => console.log(err));
        }

        fetchData();
    }, []);


    console.log(  newsList);
    return (
        <ul>
            {newsList.map(item => (
                <li key={item.title}>
                    <a href={item.url}>{item.description}</a>
                </li>
            ))}
        </ul>
    )
};
// fetch(url).then(
//     function (u) {
//         return u.json();
//     }
// ).then(
//     function (json) {
//         // console.log(111);
//         console.log(typeof json);
//         arr = json.length;
//         // .map((x) => x);
//         return json;
//     }
// ).catch(err => console.log(err))
// );
